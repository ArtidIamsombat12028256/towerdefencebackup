﻿using UnityEngine;
using System.Collections;

public class PlayerExploderEnemy : BasicEnemy {

	void updateState(){
		if (state == targetStateEnum.targetPlayer) {
			if (state != targetStateEnum.attackingBase && Vector3.Distance (endBase.transform.position, transform.position) < weaponRange) {
				state = targetStateEnum.attackingBase;
			}
		} else {
			state = targetStateEnum.targetPlayer;
		}
	}
	void stateActions(){
		switch (state) {
		case targetStateEnum.targetBase:
			break;
		case targetStateEnum.attackingBase:

			Explode ();

			break;
		}
	}
}
